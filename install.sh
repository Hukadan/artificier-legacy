#! /bin/sh

SCRIPTPATH=$(realpath $0)
SCRIPTPATH=${SCRIPTPATH%/*}

cp ${SCRIPTPATH}/src/bin/* /usr/local/bin || { echo "Problem installing binarie " ; exit 1 ; }
cp ${SCRIPTPATH}/src/etc/artificier.conf /usr/local/etc/artificier.conf.sample || { echo "Problem installing config file " ; exit 1 ; }
cp -r ${SCRIPTPATH}/src/share/* /usr/local/share || { echo "Problem installing artificier share folder" ;  exit 1 ; }

