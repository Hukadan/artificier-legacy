

# Welcome to artificier ! #

**!!! artificer is not ready yet to be used on production or even on your computer. For the moment, it should be only used for testing purposes !!!**

artificier is wrapper around poudriere and pkg. It helps you to manage pkg and poudriere together.

## Installing artificier ##

In order to install artificier, you need to clone the repo and execute the install.sh script as root.

```
% git clone https://Hukadan@bitbucket.org/Hukadan/artificier.git
% cd artificier
% sudo sh install.sh
```


## Initializing artificier ##

In order to be ready to use poudriere and pkg together, you just have to issue the following command

```
% sudo artificier init
```

This command will create a jail and a ports tree as shown bellow. As you can see, the jail name is given by the version of your FreeBSD and the ports tree name is simply *artificier*.
```
% poudriere jail -l
JAILNAME     VERSION         ARCH  METHOD TIMESTAMP           PATH
10_1-RELEASE 10.1-RELEASE-p5 amd64 ftp    2015-02-24 18:14:48 /usr/local/poudriere/jails/10_1-RELEASE
% poudriere ports -l
PORTSTREE  METHOD TIMESTAMP           PATH
artificier git    2015-02-24 18:11:11 /usr/local/poudriere/ports/artificier
```
A repo file has also been created in order for pkg to use your new repo

```
% cat /usr/local/etc/pkg/repos/FreeBSD.conf
artificier: { 
    url           : "file:///usr/local/poudriere/data/packages/10_1-RELEASE-artificier",
    enabled       : yes, 
    mirror_type   : "none"   
} 
    
FreeBSD: {   enabled: yes }

```
Before being ready, you have to upgrade all the ports installed on your system to catch up with the ports tree version. 

```
% sudo artificier upgrade
```

## Installing a package ##

In order to install a port, you need to use the *cat/port* syntax. Here is an example with the audio/murmur port :
```
% sudo artificier install audio/murmur
```
This will create the package using poudriere and then install it using pkg. You can also choose to use the **quick install mode** which allows you to install from the public repo :
```
% sudo artificier install -q audio/murmur
```
The port installed using quick install mode will be build on your local repo on the next upgrade. This allow you to install in a quick way still using custom options on the next upgrade.

## Upgrading packages ##

Upgrading the ports can be done using the following command :
```
% sudo artificier upgrade
```
## Configuration file ##

artificier can be configured through the */usr/local/etc/artificier.conf*. 


```
# You can define a few settings here.
# Name of the ports tree
# PORTSTREE_NAME=artificier
# Name of the jail in which the packages are built
# JAIL_NAME=${ARCH}-RELEASE
# Number of jails used in order to build the packages
# BUILDER_NUMBER=1
# There are three diffrent modes :
# NORMAL : everything is built using poudriere
# OPTIONS_ONLY : only ports with options defined on the make.conf file are built
# RESTRICTED_ONLY : only restricted ports are built
# ARTIFICIER_MODE=NORMAL
# If you want artificer only to build on upgrade
# ONLY_BUILD=NO
# If you want to skip some ports
# BLACK_LIST_ENABLE=NO
```

### *BUILDER_NUMBER option* ###

This option is equivalent to the ***-J*** option of the *poudriere* command. It allows you to set how many jails will be used in order to build your packages.

### *MODE option* ###

When set to *NORMAL*, all the ports are built using poudriere. When set to *RESTRICTED_ONLY*, only restricted ports (an their dependencies) are built. When set to *OPTIONS_ONLY*, only ports with settings in the make.conf file (an their dependencies) are built. The other ports are installed and upgraded using the public repo. This mode can be usefull if you want to use the public repo but also need some restricted ports. **When using this mode, you have to keep in mind that it might lead to dependency problems** (see [this thread](https://forums.freebsd.org/threads/install-some-packages-from-local-repository.49280/) and the SirDice comment). **You have been warned !!**

### *ONLY_BUILD option* ###
When set to *YES*, the *artificier upgrade* command will build ports without installing them. You will have to upgrade them using *pkg upgrade*.

## TODO ##

* Use a proper Makefile to install artificier
* Make artificier compatible with STABLE and CURRENT
* When doing *artificier install*, check if the port is already installed before anything else (with same config options)
* Update only when security updates are present
