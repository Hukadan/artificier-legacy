#!/bin/sh
#
# Copyright (c) 2014-2015 Romain Vincent <artificier@hukadan.org>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# The overall structure of the program was inspired by poudriere written by
# Baptiste Daroussin and Bryan Drewery.

usage() {
cat << EOF
Usage : artificier install [-q] cat/port 

Options:
    
    -q       install packages form public repo. the packages
             will nevertheless be built next time.

EOF
exit 1
}

PROCEED_TO_INSTALL=NO
CONTAIN_RESTRICT=NO 

SCRIPTPATH=$(realpath $0)
SCRIPTPREFIX=${SCRIPTPATH%/*}
. ${SCRIPTPREFIX}/common.sh


while getopts "q" FLAG; do
    case "$FLAG" in
        q)
            FAST_INSTALL=YES
            ;;
        *)
            usage
            ;;
    esac
done

shift $((OPTIND-1))

[ $# -lt 1 ] && usage

to_install=$@

check_if_installed(){
    for pkg_to_check in $to_install
    do
        is_not_installed && PROCEED_TO_INSTALL=YES 
    done
    return 0
}

check_if_restricted(){
    for pkg_to_check in $to_install
    do
        is_not_restricted || CONTAIN_RESTRICT=YES 
    done
    return 0
}

# if package is restricted, need to build
[ "$FAST_INSTALL" = "NO" ] || check_if_restricted
[ "$CONTAIN_RESTRICT" = "NO" ] || { echo "Restricted package(s) need building" ; exit 1 ; }
# if package installed, nothing to do
check_if_installed
[ "$PROCEED_TO_INSTALL" = "YES" ] || { echo "Package(s) already installed" ; exit 1 ; }

pkg_list="${PKG_PRESENT} ${to_install}"

# Let's go
if [ "$FAST_INSTALL" = "NO" ] ; then
    bulk_ports || { echo "Aborting while building ports" ; exit 1 ; }
fi

install_package || { echo "Aborting while installing packages" ; exit 1 ; }

exit 0
