#!/bin/sh
#
# Copyright (c) 2014-2015 Romain Vincent <artificier@hukadan.org>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# The overall structure of the program was inspired by poudriere written by
# Baptiste Daroussin and Bryan Drewery.

usage() {
cat << EOF
Usage : artificier config cat/port 

Options:
    None

EOF
exit 1
}

SCRIPTPATH=$(realpath $0)
SCRIPTPREFIX=${SCRIPTPATH%/*}
. ${SCRIPTPREFIX}/common.sh

input_options=""
CONFIG_ALL=NO

while getopts "acCnrsh" FLAG; do
    case "$FLAG" in
        a)
            CONFIG_ALL=YES
            ;;
        c)
            input_options=${input_options}c
            ;;
        C)
            input_options=${input_options}C
            ;;
        n)
            input_options=${input_options}n
            ;;
        r)
            input_options=${input_options}r
            ;;
        s)
            input_options=${input_options}s
            ;;
        h)
            usage
            ;;
        *)
            usage
            ;;
    esac
done

shift $((OPTIND-1))

if [ "$CONFIG_ALL" = "YES" ] ; then
    get_pkg_to_build
    to_config=$pkg_to_build
else
    [ ${#@} -lt 1 ] && usage
    to_config=$@
fi

[ ${#input_options} -gt 0 ] && input_options=-$input_options

CMD="poudriere -e $POUDRIERE_CONF options -p $PORTS_TREE_NAME -j $JAIL_NAME $input_options $to_config"
$CMD || { echo "Aborting while configuring" ; exit 1 ; }




