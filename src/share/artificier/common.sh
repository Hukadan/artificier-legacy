#!/bin/sh
#
# Copyright (c) 2014-2015 Romain Vincent <artificier@hukadan.org>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# The overall structure of the program was inspired by poudriere written by
# Baptiste Daroussin and Bryan Drewery.

ARCH=$(uname -p)
FREEBSD_VERSION=$(uname -r | cut -d - -f 1,2)


# POUDRIERE RELATED SETTINGS
POUDRIERE_FOLDER=/usr/local/poudriere
POUDRIERE_CONF=/usr/local/etc
MAKE_FILE=${POUDRIERE_CONF}/poudriere.d/make.conf
DIST_FILES=/usr/ports/distfiles
PORTS_TREE_NAME=artificier
# jail name cannot contain '.'
JAIL_NAME=$(echo $FREEBSD_VERSION | sed  's/\./\_/g')
BUILDER_NUMBER=1
MOUNT_POINT=/usr/ports

# PKG RELATED SETTINGS
LOCAL_ETC=/usr/local/etc
PKGCONFDIR=${LOCAL_ETC}/pkg/repos
PKG_PRESENT=$(pkg query -e '%a = 0' %o)

# ARTIFICIER RELATED SETTINGS
ONLY_BUILD=NO
FAST_INSTALL=NO

# NORMAL OR RESTRICTED_ONLY OR OPTIONS_ONLY
ARTIFICIER_MODE=NORMAL


# MISC

SEPARATOR="==================================================================="
EDITOR=vim


# LOADING CONFIG FILE

. ${LOCAL_ETC}/artificier.conf || echo "Config file not found, using default values"


# BOOLEAN FUNCTIONS

poudriere_absent(){
    pkg info -e ports-mgmt/poudriere && return 1
    return 0
}

portstree_absent(){
    poudriere -e $POUDRIERE_CONF ports -ln | grep -w "$PORTS_TREE_NAME" >/dev/null && return 1
    return 0
}

jail_absent(){
    poudriere -e $POUDRIERE_CONF jail -ln | grep -w "$JAIL_NAME" >/dev/null && return 1
    return 0
}

old_jail_present(){
    poudriere -e $POUDRIERE_CONF jail -ln | grep -w "$OLD_JAIL_NAME" >/dev/null && return 0
    return 1
}

is_not_restricted(){
    local is_restricted=$(egrep -c "${pkg_to_check}" /usr/ports/LEGAL)
    return $is_restricted
}

is_not_installed(){
    local is_installed=$(echo "$PKG_PRESENT" | egrep -c "${pkg_to_check}")
    return $is_installed
}


# POUDRIERE RELATED

install_poudriere(){
    if poudriere_absent ; then
        echo "$SEPARATOR"
        echo "poudriere is not installed. Installing now..."
        CMD="pkg install -y ports-mgmt/poudriere"
        $CMD > /dev/null || { echo "Problem while installing poudriere" ; return 1 ; }
        return 0
    else
        echo "poudriere is already installed. Skipping..."
        return 0
    fi
}

create_jail(){
    if jail_absent ; then
        echo "$SEPARATOR"
        echo "Creating the jail. This may take a while..."
        CMD="poudriere -e $POUDRIERE_CONF jail -c -j $JAIL_NAME -a $ARCH -v ${FREEBSD_VERSION}"
        $CMD > /dev/null || { echo "Problem while creating the jail" ; return 1 ; }
        return 0
    else
        echo "Jail already exists. Skipping.."
        return 0
    fi
}

bulk_ports(){
    if jail_absent ; then
        echo "$SEPARATOR"
        echo "Jail does not exist. Run 'artificier init' first"
        return 1
    else
        get_pkg_to_build || { echo "Could not get pkg_to_build" ; return 1 ; }
        echo "$SEPARATOR"
        echo "Cleaning uneeded packages..."
        CMD="poudriere -e $POUDRIERE_CONF pkgclean -j $JAIL_NAME -p $PORTS_TREE_NAME $pkg_to_build"
        $CMD > /dev/null || { echo "Problem while cleaning packages." ; }
        echo "$SEPARATOR"
        echo "Building packages..."
        echo "You can follow the building process with your browser :"
        echo "file:///usr/local/poudriere/data/logs/bulk/${JAIL_NAME}-${PORTS_TREE_NAME}/latest/build.html "
        CMD="poudriere -e $POUDRIERE_CONF bulk -J $BUILDER_NUMBER -j $JAIL_NAME -p $PORTS_TREE_NAME $pkg_to_build"
        $CMD >> /dev/null 
        [ $? -gt 0 ] && echo "Some ports did not build properly. It is expected if you use a blacklist"
        echo "$SEPARATOR"
    fi
}

create_port(){
    if portstree_absent ; then
        echo "$SEPARATOR"
        echo "Creating ports tree. This may take a while..."
        CMD="poudriere -e $POUDRIERE_CONF ports -c -f none -M $MOUNT_POINT -p $PORTS_TREE_NAME"
        $CMD  || { echo "Problem while creating ports tree" ; return 1 ; }
        return 0
    else
        echo "Ports tree present. Skipping.."
        return 0
    fi
}

upgrade_ports(){
    if portstree_absent ; then
        echo "$SEPARATOR"
        echo "Ports tree does not exists. Run 'artificier init' first"
        return 1
    else
        echo "$SEPARATOR"
        echo "Updating ports tree..."
        CMD="poudriere -e $POUDRIERE_CONF ports -p $PORTS_TREE_NAME -u"
        $CMD > /dev/null || { echo "Problem while updating ports tree." ; return 1 ; }
        return 0
    fi
}

create_distfiles(){
    [ -d $DIST_FILES ] || mkdir -p $DIST_FILES || { echo "problem while creating distfiles folder" ; return 1 ; }
}

update_jail(){
    if jail_absent ; then
        echo "$SEPARATOR"
        echo "No jail to update."
        return 1
    else
        CMD="poudriere -e $POUDRIERE_CONF jail -u -j $JAIL_NAME"
        $CMD > /dev/null || { echo "Problem while updating the jail" ; return 1 ; }
        return 0
    fi
}

delete_old_jail(){
    if old_jail_present ; then
        CMD="poudriere -e $POUDRIERE_CONF jail -d -j $old_jail_name"
        $CMD || { echo "problem while deleting the old jail" ; return 1 ; }
        return 0
    else
        echo "No old jail to delete"
        return 1
    fi
}


# PKG RELATED FUNCTIONS

create_conf_folder(){
    [ -d "$PKGCONFDIR" ] || mkdir -p $PKGCONFDIR || { echo "problem while creating pkg.conf folder" ; return 1 ; }
}

create_pkg_conf(){
    cat << EOT > $PKGCONFDIR/FreeBSD.conf
artificier: { 
    url           : "file:///usr/local/poudriere/data/packages/$JAIL_NAME-$PORTS_TREE_NAME",
    enabled       : yes, 
    mirror_type   : "none"   
} 
    
FreeBSD: { enabled: yes }
EOT
    return 0
}

install_package(){
    CMD="pkg install $to_install"
    $CMD || { echo "Problem while installing" ; return 1 ; }
}

check_updating(){
    echo "Checking UPDATING file..."
    last_upgrade=$(pkg query -a '%t' | sort | tail -1)  || { echo "Problem getting date while checking UPDATING!" ; return 1 ; }
    last_upgrade=$(date -r $last_upgrade +"%Y%m%d") || { echo "Problem converting date while checking UPDATING!" ; return 1 ; }
    updating_file=${POUDRIERE_FOLDER}/ports/${PORTS_TREE_NAME}/UPDATING
    pkg_updating=$(pkg updating -d $last_upgrade) || { echo "Problem while checking UPDATING file!" ; return 1 ; }
    return 0
}

show_updating(){
    check_updating || { echo "Aborting.." ; return 1 ; }
    local line_number=$(echo "${pkg_updating}" | wc -l)
    echo "$line_number"
    if [ ${line_number} -gt 1 ] ; then
        echo "--------------------------------------" 
        echo
        echo
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo "    PLEASE READ CAREFULLY             "
        echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        echo 
        echo
        echo "--------------------------------------" 
        echo
        echo "$pkg_updating"
        echo
        echo "--------------------------------------" 
        while true ; do
            echo -n "Have you anything to deal with in this message ? 1: YES / 2: NO -> "
            read REPLY
            case $REPLY in
                1)
                    BUILD_ONLY=YES
                    return 0
                    ;;
                2)
                    return 0
                    ;;
                *)
                    ;;
            esac
        done

    fi
    return 0
}

upgrade_installed(){
    CMD="pkg upgrade"
    $CMD || { echo "Problem while upgrading" ; return 1 ; }
    return 0
}


# MISC

create_pkg_with_options_list(){
    pkg_with_options=$( egrep -v '#' < $MAKE_FILE) 
    pkg_with_options=$( echo "$pkg_with_options" | egrep "SET|UNSET") || { echo "make.conf not found or empty. Nothing to build" ; return 1 ; }
    pkg_with_options=$( echo "$pkg_with_options" | awk -F_ '{ printf "%s/%s" , $1, $2 ; printf "\n" }' )
    pkg_with_options=$( echo "$pkg_with_options" | sort -u)
    echo "Ports with options :"
    echo "$pkg_with_options"
}

create_pkg_restricted_list(){
    pkg_restricted=""
    for pkg_to_check in $pkg_list
    do
        is_not_restricted || pkg_restricted="${pkg_restricted} ${pkg_to_check}" || { echo "Problem with restricted list" ; return 1 ; }
    done
    return 0

}

get_pkg_to_build(){
    # GIVE A PKG_TO_BUILD LIST ACCORDING TO THE MODE
    case "$ARTIFICIER_MODE" in
        "NORMAL")
            pkg_to_build=$pkg_list
            return 0
            ;;
        "OPTIONS_ONLY")
            create_pkg_with_options_list || { echo "Skipping building..." ; return 1 ; }
            pkg_to_build=$pkg_with_options
            return 0
            ;;
        "RESTRICTED_ONLY")
            create_pkg_restricted_list || { echo "Skipping building..." ; return 1 ; }
            pkg_to_build=$pkg_restricted
            return 0
            ;;
        *)
            echo "Unknown mode"
            return 1
            ;;
    esac
}

