#!/bin/sh
#
# Copyright (c) 2014-2015 Romain Vincent <artificier@hukadan.org>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# The overall structure of the program was inspired by poudriere written by
# Baptiste Daroussin and Bryan Drewery.


usage() {
cat << EOF
Usage : artificier update [-p] 

Options:
    
    -p     use after a minor or major
           upgrade giving the name of 
           the previous version

EOF
exit 1
}

SCRIPTPATH=$(realpath $0)
SCRIPTPREFIX=${SCRIPTPATH%/*}
. ${SCRIPTPREFIX}/common.sh

jail_upgrade=NO

while getopts "p:h" FLAG; do
    case "$FLAG" in
        p)
            old_jail_name=$(echo $OPTARG | sed 's/\./\_/g')
            jail_upgrade=YES
            ;;
        h)
            usage
            ;;
        *)
            usage
            ;;
    esac
done

shift $((OPTIND-1))


# Let's go 
if [ "$jail_upgrade" = "YES" ] ; then
    delete_old_jail && create_jail || { echo "Aborting while swapping jails" ; exit 1 ; }
else
    update_jail || { echo "Aborting while updating jail" ; exit 1 ; }
fi
exit 0
