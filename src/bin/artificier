#!/bin/sh
#
# Copyright (c) 2014-2015 Romain Vincent <artificier@hukadan.org>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
#
# The overall structure of the program was inspired by poudriere written by
# Baptiste Daroussin and Bryan Drewery.

ARTIFICIER_VERSION="0.1Alpha"

usage(){
    cat << EOF
Usage : artificier [-d] command [options]

Options:
    -d             -- Dry run

Commands:
    version        -- Show the version of artificier
    help           -- Show usage
    init           -- Install poudriere, set up jail and fetch ports tree
    install        -- Install packages
    upgrade        -- Upgrade packages
    config         -- Open make.conf file                       
    update         -- Update the jail 

EOF
       exit 1
}

DRY_RUN="no"

while getopts "d" FLAG; do
    case "${FLAG}" in
        d)
            DRY_RUN="yes"
            ;;
        *)
            usage
            ;;
    esac
done

shift $((OPTIND-1))

ARTIFICIERPATH=`realpath $0`
ARTIFICIERPREFIX=${ARTIFICIERPATH%\/bin/*}
ARTIFICIERPREFIX=${ARTIFICIERPREFIX}/share

CMD=$1
shift
CMD_ENV="PATH=${PATH} ARTIFICIER_VERSION=${ARTIFICIER_VERSION} DRY_RUN=${DRY_RUN}"

case "${CMD}" in
    info|init|upgrade|install|config|update|test)
        ;;
    version)
        echo "${ARTIFICIER_VERSION}"
        exit 0
        ;;
    help)
        usage
        ;;
    *)
        echo "Unknown command '${CMD}'"
        usage
        ;;
esac

exec env -i ${CMD_ENV} /bin/sh "${ARTIFICIERPREFIX}/artificier/${CMD}.sh" $@
